<?php
session_start();
// Change this to your connection info.
$adServer = "ldapi:///";
$ldap = ldap_connect($adServer);
$username = $_POST['email'];
$password = $_POST['password'];
ldap_set_option($ldap, LDAP_OPT_PROTOCOL_VERSION, 3);
ldap_set_option($ldap, LDAP_OPT_REFERRALS, 0);

if ( !isset($_POST['email'], $_POST['password']) ) {
	// Could not get the data that should have been sent.
    $datasend['info'] = '無法取得帳號密碼';
    echo json_encode($datasend, JSON_UNESCAPED_UNICODE);
}

if(isset($_POST['email']) && isset($_POST['password'])){
    $filter="(uid=$username)";
    $result = ldap_search($ldap,"dc=ntuhort,dc=ddns,dc=net",$filter);
    ldap_sort($ldap,$result,"cn");
    $info = ldap_get_entries($ldap, $result);
    
    if (!is_null($info[0])){
        //$ldaprdn = 'ntuhort.ddns.net/users' . "\\" . $identname;
        $ldaprdn = $info[0]["dn"];
        $bind = @ldap_bind($ldap, $ldaprdn, $password);

        if ($bind) {
            $filter="(uid=$username)";
            $result = ldap_search($ldap,"dc=ntuhort,dc=ddns,dc=net",$filter);
            ldap_sort($ldap,$result,"uidnumber");
            $info = ldap_get_entries($ldap, $result);
    
            session_regenerate_id();
            $_SESSION['loggedin'] = TRUE;
            $_SESSION['name'] = $_POST['email'];
            $_SESSION['id'] = $info[0]["uidnumber"][0];
            $_SESSION['realname'] = $info[0]["sn"][0];
            $datasend['value'] = "0";
            $datasend['info'] = '';
            @ldap_close($ldap);
            
            echo json_encode($datasend, JSON_UNESCAPED_UNICODE);
            
        } else {
            $datasend['info'] = '密碼錯誤';
            echo json_encode($datasend, JSON_UNESCAPED_UNICODE);
        }

    } else {
        $datasend['info'] = '帳號錯誤';
        echo json_encode($datasend, JSON_UNESCAPED_UNICODE);
    }
    

}



?>