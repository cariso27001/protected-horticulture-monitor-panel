<?php
session_start();
// DB table to use
$table = 'WeatherStation';
 
// Table's primary key
$primaryKey = 'id';
 
// Array of database columns which should be read and sent back to DataTables.
// The `db` parameter represents the column name in the database, while the `dt`
// parameter represents the DataTables column identifier. In this case simple
// indexes
$columns = array(
    array( 'db' => 'id', 'dt' => 0 ),
    array( 'db' => 'user', 'dt' => 1 ),
    array( 'db' => 'Temperature',  'dt' => 2 ),
    array( 'db' => 'Humidity',   'dt' => 3 ),
    array( 'db' => 'Light',     'dt' => 4 ),
    array( 'db' => 'Soil',     'dt' => 5 ),
    array( 'db' => 'Precipitation',     'dt' => 6 ),
    array( 'db' => 'Evaporation',     'dt' => 7 ),
    array( 'db' => 'reading_time',     'dt' => 8 )
);
 
// SQL server connection information
$sql_details = array(
    'user' => 'tydares001',
    'pass' => 'wufengep8260AS#',
    'db'   => 'IACMS',
    'host' => 'localhost'
);

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * If you just want to use the basic configuration for DataTables with PHP
 * server-side, there is no need to edit below this line.
 */
 
require( 'ssp.class.php' );
 
echo json_encode(
    SSP::complex( $_GET, $sql_details, $table, $primaryKey, $columns)
);