<?php
session_start();
// Change this to your connection info.
$DATABASE_HOST = 'localhost';
$DATABASE_USER = 'tydares001';
$DATABASE_PASS = 'wufengep8260AS#';
$DATABASE_NAME = 'IACMS';
$datasend = array();
$datasend['value'] = '1';
function random_str(
    int $length = 64,
    string $keyspace = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'
): string {
    if ($length < 1) {
        throw new \RangeException("Length must be a positive integer");
    }
    $pieces = [];
    $max = mb_strlen($keyspace, '8bit') - 1;
    for ($i = 0; $i < $length; ++$i) {
        $pieces []= $keyspace[random_int(0, $max)];
    }
    return implode('', $pieces);
}

// Try and connect using the info above.
$con = mysqli_connect($DATABASE_HOST, $DATABASE_USER, $DATABASE_PASS, $DATABASE_NAME);
if (mysqli_connect_errno()) {
	// If there is an error with the connection, stop the script and display the error.
	$datasend['info'] = 'MySQL資料庫連線錯誤: ' . mysqli_connect_error();
	echo json_encode($datasend, JSON_UNESCAPED_UNICODE);
}

// Now we check if the data was submitted, isset() function will check if the data exists.
if (!isset($_POST['mac'], $_POST['name'], $_POST['location'], $_POST['functions'])) {
	// Could not get the data that should have been sent.
	$datasend['info'] = '請確認正確填寫所有欄位';
	echo json_encode($datasend, JSON_UNESCAPED_UNICODE);
}
// Make sure the submitted registration values are not empty.
if (empty($_POST['mac']) || empty($_POST['name']) || empty($_POST['location']) || empty($_POST['functions'])) {
	// One or more values are empty.
	$datasend['info'] = '請確認正確填寫所有欄位';
	echo json_encode($datasend, JSON_UNESCAPED_UNICODE);
} else {
// We need to check if the account with that username exists.
	if ($stmt = $con->prepare('SELECT id, name FROM regdevice WHERE mac_address = ?')) {
		// Bind parameters (s = string, i = int, b = blob, etc), hash the password using the PHP password_hash function.
		$stmt->bind_param('s', $_POST['mac']);
		$stmt->execute();
		$stmt->store_result();
		// Store the result so we can check if the account exists in the database.
		if ($stmt->num_rows > 0) {
			// Username already exists
			$datasend['info'] = '裝置已被註冊，請確認或更換MAC地址';
			echo json_encode($datasend, JSON_UNESCAPED_UNICODE);
		} else {
			// Username doesnt exists, insert new account
			if ($stmt = $con->prepare('INSERT INTO regdevice (`mac_address`, `name`, `user`,`location`, `function`, `api_key`) VALUES (?, ?, ?, ?, ?, ?)')) {
				// We do not want to expose passwords in our database, so hash the password and use password_verify when a user logs in.
				// $api_key = password_hash($_POST['mac'], PASSWORD_DEFAULT);
				$api_key = random_str(16);
				$function = '氣象監控';
				$stmt->bind_param('ssssss', $_POST['mac'], $_POST['name'], $_SESSION['name'], $_POST['location'], $_POST['functions'], $api_key);
				$stmt->execute();
				$datasend['value'] = "0";
				$datasend['info'] = '註冊成功，請記下API：' . $api_key;
				echo json_encode($datasend, JSON_UNESCAPED_UNICODE);
			} else {
				// Something is wrong with the sql statement, check to make sure accounts table exists with all 3 fields.
				$datasend['info'] = '資料庫寫入錯誤';
				echo json_encode($datasend, JSON_UNESCAPED_UNICODE);
			}
		}
		$stmt->close();
	} else {
		// Something is wrong with the sql statement, check to make sure accounts table exists with all 3 fields.
		$datasend['info'] = '資料庫寫入錯誤';
		echo json_encode($datasend, JSON_UNESCAPED_UNICODE);
	}
}
$con->close();
?>