<?php
require 'mysqlconn.php';
$sql = "((
    SELECT IFNULL((
    SELECT Precipitation
    FROM WeatherStation
    WHERE (DATE(reading_time) = CURDATE()) AND $user_filter AND ((TIME(reading_time)<='04:00:00'))
    ORDER BY ABS(TIMEDIFF(TIME(reading_time), '03:00:00'))
    LIMIT 1),0) AS Precipitation, IFNULL((
    SELECT Evaporation
    FROM WeatherStation
    WHERE (DATE(reading_time) = CURDATE()) AND $user_filter AND ((TIME(reading_time)<='04:00:00'))
    ORDER BY ABS(TIMEDIFF(TIME(reading_time), '03:00:00'))
    LIMIT 1),0) AS Evaporation) UNION ALL
    (
    SELECT IFNULL((
    SELECT Precipitation
    FROM WeatherStation
    WHERE (DATE(reading_time) = CURDATE()) AND $user_filter AND ((TIME(reading_time)<='07:00:00') AND (TIME(reading_time)>='05:00:00'))
    ORDER BY ABS(TIMEDIFF(TIME(reading_time), '06:00:00'))
    LIMIT 1),0) AS Precipitation, IFNULL((
    SELECT Evaporation
    FROM WeatherStation
    WHERE (DATE(reading_time) = CURDATE()) AND $user_filter AND ((TIME(reading_time)<='07:00:00') AND (TIME(reading_time)>='05:00:00'))
    ORDER BY ABS(TIMEDIFF(TIME(reading_time), '06:00:00'))
    LIMIT 1),0) AS Evaporation) UNION ALL
    (
    SELECT IFNULL((
    SELECT Precipitation
    FROM WeatherStation
    WHERE (DATE(reading_time) = CURDATE()) AND $user_filter AND ((TIME(reading_time)<='10:00:00') AND (TIME(reading_time)>='08:00:00'))
    ORDER BY ABS(TIMEDIFF(TIME(reading_time), '09:00:00'))
    LIMIT 1),0) AS Precipitation, IFNULL((
    SELECT Evaporation
    FROM WeatherStation
    WHERE (DATE(reading_time) = CURDATE()) AND $user_filter AND ((TIME(reading_time)<='10:00:00') AND (TIME(reading_time)>='08:00:00'))
    ORDER BY ABS(TIMEDIFF(TIME(reading_time), '09:00:00'))
    LIMIT 1),0) AS Evaporation) UNION ALL
    (
    SELECT IFNULL((
    SELECT Precipitation
    FROM WeatherStation
    WHERE (DATE(reading_time) = CURDATE()) AND $user_filter AND ((TIME(reading_time)<='13:00:00') AND (TIME(reading_time)>='11:00:00'))
    ORDER BY ABS(TIMEDIFF(TIME(reading_time), '12:00:00'))
    LIMIT 1),0) AS Precipitation, IFNULL((
    SELECT Evaporation
    FROM WeatherStation
    WHERE (DATE(reading_time) = CURDATE()) AND $user_filter AND ((TIME(reading_time)<='13:00:00') AND (TIME(reading_time)>='11:00:00'))
    ORDER BY ABS(TIMEDIFF(TIME(reading_time), '12:00:00'))
    LIMIT 1),0) AS Evaporation) UNION ALL
    (
    SELECT IFNULL((
    SELECT Precipitation
    FROM WeatherStation
    WHERE (DATE(reading_time) = CURDATE()) AND $user_filter AND ((TIME(reading_time)<='16:00:00') AND (TIME(reading_time)>='14:00:00'))
    ORDER BY ABS(TIMEDIFF(TIME(reading_time), '15:00:00'))
    LIMIT 1),0) AS Precipitation, IFNULL((
    SELECT Evaporation
    FROM WeatherStation
    WHERE (DATE(reading_time) = CURDATE()) AND $user_filter AND ((TIME(reading_time)<='16:00:00') AND (TIME(reading_time)>='14:00:00'))
    ORDER BY ABS(TIMEDIFF(TIME(reading_time), '15:00:00'))
    LIMIT 1),0) AS Evaporation) UNION ALL
    (
    SELECT IFNULL((
    SELECT Precipitation
    FROM WeatherStation
    WHERE (DATE(reading_time) = CURDATE()) AND $user_filter AND ((TIME(reading_time)<='19:00:00') AND (TIME(reading_time)>='17:00:00'))
    ORDER BY ABS(TIMEDIFF(TIME(reading_time), '18:00:00'))
    LIMIT 1),0) AS Precipitation, IFNULL((
    SELECT Evaporation
    FROM WeatherStation
    WHERE (DATE(reading_time) = CURDATE()) AND $user_filter AND ((TIME(reading_time)<='19:00:00') AND (TIME(reading_time)>='17:00:00'))
    ORDER BY ABS(TIMEDIFF(TIME(reading_time), '18:00:00'))
    LIMIT 1),0) AS Evaporation) UNION ALL
    (
    SELECT IFNULL((
    SELECT Precipitation
    FROM WeatherStation
    WHERE (DATE(reading_time) = CURDATE()) AND $user_filter AND ((TIME(reading_time)<='22:00:00') AND (TIME(reading_time)>='20:00:00'))
    ORDER BY ABS(TIMEDIFF(TIME(reading_time), '21:00:00'))
    LIMIT 1),0) AS Precipitation, IFNULL((
    SELECT Evaporation
    FROM WeatherStation
    WHERE (DATE(reading_time) = CURDATE()) AND $user_filter AND ((TIME(reading_time)<='22:00:00') AND (TIME(reading_time)>='20:00:00'))
    ORDER BY ABS(TIMEDIFF(TIME(reading_time), '21:00:00'))
    LIMIT 1),0) AS Evaporation) UNION ALL
    (
    SELECT IFNULL((
    SELECT Precipitation
    FROM WeatherStation
    WHERE (DATE(reading_time) = CURDATE()) AND $user_filter AND (TIME(reading_time)>='23:00:00')
    ORDER BY ABS(TIMEDIFF(TIME(reading_time), '23:59:58'))
    LIMIT 1),0) AS Precipitation, IFNULL((
    SELECT Evaporation
    FROM WeatherStation
    WHERE (DATE(reading_time) = CURDATE()) AND $user_filter AND (TIME(reading_time)>='23:00:00')
    ORDER BY ABS(TIMEDIFF(TIME(reading_time), '23:59:58'))
    LIMIT 1),0) AS Evaporation))";
$result = $conn->query($sql);


while($row = $result -> fetch_assoc()) {    
    $array_values[] = $row;
};

echo json_encode($array_values);
$conn->close();
?>