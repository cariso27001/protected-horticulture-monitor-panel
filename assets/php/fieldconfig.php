<?php
session_start();
$DATABASE_HOST = 'localhost';
$DATABASE_USER = 'tydares001';
$DATABASE_PASS = 'wufengep8260AS#';
$DATABASE_NAME = 'IACMS';
$datasend = "";
$con = mysqli_connect($DATABASE_HOST, $DATABASE_USER, $DATABASE_PASS, $DATABASE_NAME);
if (mysqli_connect_errno()) {
	// If there is an error with the connection, stop the script and display the error.
	$datasend = 'MySQL資料庫連線錯誤: ' . mysqli_connect_error();
	echo json_encode($datasend, JSON_UNESCAPED_UNICODE);
}

// Now we check if the data was submitted, isset() function will check if the data exists.
if (!isset($_POST['ValveDuration'], $_POST['SoilThres'], $_POST['Interval'])) {
	// Could not get the data that should have been sent.
	$datasend = '請確認正確填寫所有欄位';
	echo json_encode($datasend, JSON_UNESCAPED_UNICODE);
}
// Make sure the submitted registration values are not empty.
if (empty($_POST['ValveDuration']) || empty($_POST['SoilThres']) || empty($_POST['Interval'])) {
	// One or more values are empty.
	$datasend = '請確認正確填寫所有欄位';
	echo json_encode($datasend, JSON_UNESCAPED_UNICODE);
}
elseif (!is_numeric($_POST['ValveDuration']) || !is_numeric($_POST['SoilThres']) || !is_numeric($_POST['Interval'])) {
	$datasend = '請輸入數值';
	echo json_encode($datasend, JSON_UNESCAPED_UNICODE);
} 
else {
	if ($stmt = $con->prepare('INSERT INTO FieldConfig (`user`, `ValveDuration`, `SoilThres`, `Interval`) VALUES (?, ?, ?, ?)')) {
		$stmt->bind_param('ssss', $_SESSION['name'], $_POST['ValveDuration'], $_POST['SoilThres'], $_POST['Interval']);
		$stmt->execute();
		$datasend = '更新成功';
		echo json_encode($datasend, JSON_UNESCAPED_UNICODE);
	} else {
		// Something is wrong with the sql statement, check to make sure accounts table exists with all 3 fields.
		$datasend = '資料庫寫入錯誤';
		echo json_encode($datasend, JSON_UNESCAPED_UNICODE);
	}
}
$con->close();
?>