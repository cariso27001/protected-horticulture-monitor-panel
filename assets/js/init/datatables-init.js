(function ($) {
    //    "use strict";


    /*  Data Table
    -------------*/




    $('#bootstrap-data-table').DataTable({
        lengthMenu: [[10, 20, 50, -1], [10, 20, 50, "All"]],
    });



    $('#bootstrap-data-table-export').DataTable({
		dom: "<'row'<'fss'B>>" +
			 "<'row'<'col-sm-12 col-md-6'l><'col-sm-12 col-md-6'f>>" + 'rt' +
			 "<'row'<'col-sm-12 col-md-6'i><'col-sm-12 col-md-6'p>>",
        lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
        buttons: [
		   {extend: 'copy', text: '複製', className: "btn btn-light"}, 
		   {extend: 'csv', text: 'CSV', className: "btn btn-light"},
		   {extend: 'excel', text: 'Excel', className: "btn btn-light"},
		   {extend: 'pdf', text: 'PDF', className: "btn btn-light"},
		   {extend: 'print', text: '列印', className: "btn btn-light"}
		],
		"language": {
			"processing":   "處理中...",
			"loadingRecords": "載入中...",
			"lengthMenu":   "顯示 _MENU_ 項結果",
			"zeroRecords":  "沒有符合的結果",
			"info":         "顯示第 _START_ 至 _END_ 項結果，共 _TOTAL_ 項",
			"infoEmpty":    "顯示第 0 至 0 項結果，共 0 項",
			"infoFiltered": "(從 _MAX_ 項結果中過濾)",
			"infoPostFix":  "",
			"search":       "搜尋:",
			"paginate": {
				"first":    "第一頁",
				"previous": "上一頁",
				"next":     "下一頁",
				"last":     "最後一頁"
			},
			"aria": {
				"sortAscending":  ": 升冪排列",
				"sortDescending": ": 降冪排列"
			}
		 }
	});
	
	$('#row-select').DataTable( {
			initComplete: function () {
				this.api().columns().every( function () {
					var column = this;
					var select = $('<select class="form-control"><option value=""></option></select>')
						.appendTo( $(column.footer()).empty() )
						.on( 'change', function () {
							var val = $.fn.dataTable.util.escapeRegex(
								$(this).val()
							);
	 
							column
								.search( val ? '^'+val+'$' : '', true, false )
								.draw();
						} );
	 
					column.data().unique().sort().each( function ( d, j ) {
						select.append( '<option value="'+d+'">'+d+'</option>' )
					} );
				} );
			}
		} );






})(jQuery);