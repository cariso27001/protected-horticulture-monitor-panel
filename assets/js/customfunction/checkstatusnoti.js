var popuptank = 0;
var notitank = 0;

setTimeout(function () {
    getTanknoti();
}, 200);

$(function () {
    setInterval(getTanknoti, 3000);
})

function getTanknoti() {
    $.getJSON('assets/php/dashrtTank.php', function (data) {
        var watertankker = data
        if (watertankker.WaterTank === 'FULL') {
            if (popuptank != 1) {
                Messenger().post({
                    message: '水塔已經完成補水！',
                    type: 'success',
                    showCloseButton: true,
                    hideAfter: 5
                });
                popuptank = 1;
                notifylist();
            }
            $.getJSON('assets/php/notification.php', function (data) {
                if (data == null) {
                    var messages = "水塔滿水";
                    var level = "success";
                    $.post("assets/php/postnoti.php",
                        { messages: messages, level: level },
                    )
                }
                else if (data[0]["messages"] === "水塔滿水") {

                }
                else {
                    var messages = "水塔滿水";
                    var level = "success";
                    $.post("assets/php/postnoti.php",
                        { messages: messages, level: level },
                    )
                }
            })
        }
        else if (watertankker.WaterTank === 'LOW') {
            if (popuptank != 2) {
                Messenger().post({
                    message: '水塔無水，請速速檢查！',
                    type: 'error',
                    showCloseButton: true,
                    hideAfter: 5
                });
                popuptank = 2;
                notifylist();
            }

            $.getJSON('assets/php/notification.php', function (data) {
                if (data == null) {
                    var messages = "水塔缺水";
                    var level = "danger";
                    $.post("assets/php/postnoti.php",
                        { messages: messages, level: level },
                    )
                }
                else if (data[0]["messages"] === "水塔缺水") {

                }
                else {
                    var messages = "水塔缺水";
                    var level = "danger";
                    $.post("assets/php/postnoti.php",
                        { messages: messages, level: level },
                    )
                }
            })

        }
        else {
            if (popuptank != 3) {
                Messenger().post({
                    message: '無法取得水塔狀態！',
                    type: 'info',
                    showCloseButton: true,
                    hideAfter: 5
                });
                popuptank = 3;
                notifylist();
            }

            $.getJSON('assets/php/notification.php', function (data) {
                if (data == null) {
                    var messages = "無法取得水塔狀態";
                    var level = "warning";
                    $.post("assets/php/postnoti.php",
                        { messages: messages, level: level },
                    )
                }
                else if (data[0]["messages"] === "無法取得水塔狀態") {

                }
                else {
                    var messages = "無法取得水塔狀態";
                    var level = "warning";
                    $.post("assets/php/postnoti.php",
                        { messages: messages, level: level },
                    )
                }
            })


        }
    })
}