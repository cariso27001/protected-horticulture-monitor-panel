var getDasSeriesprecip = [];
var getDasSeriesevapo = [];
var popuptank = 0;

setTimeout(function () {
    getRecord();
    getWater();
    getTank();
    dashrt2chart();
    getfieldrt();
}, 200);

$(function () {
    setInterval(getRecord, 3000);
    setInterval(getWater, 3000);
    setInterval(getTank, 3000);
    setInterval(dashrt2chart, 18000);
    setInterval(getfieldrt, 6000);
})

//即時監控第一排數據更新
function getRecord() {
    $.getJSON('assets/php/dashrtRecord.php', function (data) {
        $('#temperature').text(data['Temperature'])
        $('#humidity').text(data['Humidity'])
        $('#light').text(data['Light'])
        $('#soil').text(data['Soil'])
    })
}

//即時監控第二排右側數據更新
function getWater() {
    $.getJSON('assets/php/dashrtWater.php', function (data) {
        $('#precipitation').text(data['Precipitation'])
        $('#evaporation').text(data['Evaporation'])
        var preciper = data['Precipitation'] / 3000 * 100
        document.getElementById('precipitation_perc').style.width = preciper + "%"
        var evaoper = data['Evaporation'] / 3000 * 100
        document.getElementById('evaporation_perc').style.width = evaoper + "%"
        $('#soil_av').text(data['AVG(Soil)'])
    })
}

//即時監控水塔狀況
function getTank() {
    $.getJSON('assets/php/dashrtTank.php', function (data) {
        var watertankker = data
        if (watertankker.WaterTank === 'FULL') {
            $('#tankchange').html('<div class="mx-auto d-block text-success"><i class="fa fa-check mx-auto d-block text-sm-center fa-5x"></i><h1 class="text-sm-center mt-2 mb-1">水塔有水</h1></div>');
            
        }
        else if (watertankker.WaterTank === 'LOW') {
            $('#tankchange').html('<div class="mx-auto d-block text-danger"><i class="fa fa-times mx-auto d-block text-sm-center fa-5x"></i><h1 class="text-sm-center mt-2 mb-1">水塔無水</h1></div>');
        }
        else {
            $('#tankchange').html('<div class="mx-auto d-block"><i class="fa fa-question-circle mx-auto d-block text-sm-center fa-5x"></i><h1 class="text-sm-center mt-2 mb-1">讀取中</h1></div>');
            
        }
    })
}



function getfieldrt() {
    var tableData = "";
    $.getJSON('assets/php/dashrtField.php', function (data) {
        console.log(data)
        for (var i = 0; i < data.length; i++) {
            tableData += "<tr>"
            tableData += "<td>" + (i + 1) + "</td>"
            tableData += "<td>" + data[i]["name"] + "</td>"
            tableData += "<td>" + data[i]["IrriLocation"] + "</td>"
            tableData += "<td>" + data[i]["IrriSoil"] + "</td>"
            tableData += "<td>" + data[i]["LastIrriTime"] + "</td>"
            tableData += "</tr>"
        }
        $('#fieldTableData').html(tableData)
    })
}

//即時監控第二排及時變化趨勢更新
function dashrt2chart() {
    getDasSeriesprecip = [];
    getDasSeriesevapo = [];
    getDasSeriesprecip.push(0);
    getDasSeriesevapo.push(0);
    $.getJSON('assets/php/dashrt2chart.php', function (data) {
        for (var i = 0, len = data.length; i < len; i++) {
            getDasSeriesprecip.push(parseFloat(data[i]['Precipitation']));
            getDasSeriesevapo.push(parseFloat(data[i]['Evaporation']));
            console.log(getDasSeriesprecip);
        }
        if ($('#traffic-chart').length) {
            var chart = new Chartist.Line('#traffic-chart', {
                labels: ['00:00', '03:00', '06:00', '09:00', '12:00', '15:00', '18:00', '21:00', '24:00'],
                series: [getDasSeriesprecip, getDasSeriesevapo]
            }, {
                low: 0,
                showArea: true,
                showLine: false,
                showPoint: false,
                fullWidth: true,
                axisY: { showLabel: true, scaleMinSpace: 30 },
                axisX: {
                    showGrid: true
                }
            });

            chart.on('draw', function (data) {
                if (data.type === 'line' || data.type === 'area') {
                    // data.element.animate({
                    //     d: {
                    //         begin: 2000 * data.index,
                    //         dur: 2000,
                    //         from: data.path.clone().scale(1, 0).translate(0, data.chartRect.height()).stringify(),
                    //         to: data.path.clone().stringify(),
                    //         easing: Chartist.Svg.Easing.easeOutQuint
                    //     }
                    // });
                }
            });
        }
    })
}