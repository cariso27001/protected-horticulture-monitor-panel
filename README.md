# Protected Horticulture Monitor Panel
An integrted monitor panel of intelligent horticulture control system.

### TOC
- [Built With](#built-with)
- [Changelog](#changelog)
- [Source Materials License](#license)

### Built With
- [ElaAdmin](https://github.com/puikinsh/ElaAdmin)
- [Ecology Filled Flat Colors](https://www.iconfinder.com/iconsets/eco-flat-2)
- [MDB Bootstrap Slider](https://mdbootstrap.com/docs/jquery/forms/slider/)

### Changelog
#### V 1.0.0
Initial Release

### Source Materials License

#### ElaAdmin License
[Colorlib](https://colorlib.com)
ElaAdmin is licensed under The MIT License (MIT). Which means that you can use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the final products. But you always need to state that Colorlib is the original author of this template.

#### Ecology Filled Flat Colors License
[Ibrandify](http://www.ibrandify.com/)
